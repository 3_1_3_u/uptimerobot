.. UptimeRobot API Python Library documentation master file, created by
   sphinx-quickstart on Wed Jan  4 13:02:01 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UptimeRobot API Python Library's documentation!
==========================================================

The UptimeRobot_ API Python Library aims to be a comprehensive and pythonic
library for accessing the `UptimeRobot API`_.  Currently, UptimeRobot has two
versions of their API, v1_ and v2_.  Version 1, v1_, is currently the default
API.  Version 2, v2_, is in beta.  This library supports both.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   dev/index

.. links

.. _UptimeRobot: https://www.uptimerobot.com
.. _`UptimeRobot API`: https://www.uptimerobot.com/api
.. _v1: https://www.uptimerobot.com/api
.. _v2: https://www.uptimerobot.com/apiv2

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

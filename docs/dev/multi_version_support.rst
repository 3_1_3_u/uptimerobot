:Author: John Hampton
:Date: 1/4/2017

================================
Supporting Multiple API versions
================================

From a user perspective, using API v1_ or v2_ should be as easy as specifying
the API version desired when creating the API object.  This should look like:

.. code-block:: py

   >>> from uptimerobot import UptimeRobot
   >>> api = UptimeRobot(apiKey='<my api key>', version=2)

The use can then call whatever API methods they desire.

Trying to handle multiple APIs in this manner presents us with various problems:

* The different API versions don't have API method parity.
* Return values are different for some of the same methods.
* New object types in API v2
* Reusing code and dynamic dispatch to v1 or v2.

Different Versions, Different Methods
-------------------------------------

The goal for supporting multiple versions of the API isn't to be able to change
between the two without any code changes in the client.  The user has to make a
conscious decision which API version they would like to use.  This, hopefully,
means they have read the API documentation and know which methods to call.

In the case that they call a method for API v2 when using API v1, if that method
doesn't exist in v1, then a NotImplemented exception can be raised.

While the method parameters for the API calls vary between v1 and v2, most
differences are in using underscores instead of camelCase or extra parameters.
This library doesn't need to hold to either convention [#]_ and can easily
provide a single interface for the common parameters.  For those that are unique
to a given API version, it is easy enough for the actual implementation to check
method signature and raise an exception upon a mismatch.  Most methods from v1
can be used in v2 w/o change.

Differing Return Values
-----------------------


.. rubric:: Footnotes

.. [#] Parameter convention will be similar to v2_ with underscores.


.. links

.. _v1: https://www.uptimerobot.com/api
.. _v2: https://www.uptimerobot.com/apiv2

# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 John Hampton <pacopablo@pacopablo.com>
# All rights reserved.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution.
#
# Author: John Hampton <pacopablo@pacopablo.com>

from setuptools import setup, find_packages

__VERSION__ = '0.0.1.0'

requires = [
    'Requests',
]

test_requires = [
    'pytest',
    'requests_mock',
]

setup(name='uptimerobot',
      version=__VERSION__,
      description='UptimeRobot API',
      long_description='UptimeRobot API (https://uptimerobot/api)',
      classifiers=[
        "Programming Language :: Python",
        ],
      author='John Hampton',
      author_email='pacopablo@pacopablo.com',
      url='',
      keywords='uptimerobot api',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      setup_requires=['pytest-runner'],
      tests_require=test_requires,
      test_suite='uptimerobot',
      install_requires=requires,
#      entry_points="""\
#      [console_scripts]
#      """,
)

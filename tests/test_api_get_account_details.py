# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 John Hampton <pacopablo@pacopablo.com>
# All rights reserved.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution.
#
# Author: John Hampton <pacopablo@pacopablo.com>

import requests_mock
from uptimerobot import UptimeRobot

TEST_API_KEY = 'u956-afus321g565fghr519'


def test_api_v1_get_account_details():
    """ Test Account object instantiation """

    account_details = """
    {
        "stat": "ok",
        "account": {
            "monitorLimit": "100",
            "monitorInterval": "1",
            "upMonitors": "35",
            "downMonitors": "9",
            "pausedMonitors": "11"
        }
    }"""[1:]

    with requests_mock.Mocker() as m:
        m.get('https://api.uptimerobot.com/getAccountDetails?apiKey={}&noJsonCallback=1&format=json'.format(TEST_API_KEY),
              text=account_details, status_code=200,
              )

        api = UptimeRobot(TEST_API_KEY, version=1)
        acct = api.get_account_details()
        assert acct.status() == 'ok'
        assert acct.up() == 35
        assert acct.down() == 9
        assert acct.interval() == 1
        assert acct.limit() == 100
        assert acct.paused() == 11
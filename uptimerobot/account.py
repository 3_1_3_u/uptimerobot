# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 John Hampton <pacopablo@pacopablo.com>
# All rights reserved.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution.
#
# Author: John Hampton <pacopablo@pacopablo.com>

__all__ = [
    'Account'
]


class Account(object):
    """ Class describing an UptimeRobot Account

    .. TODO: do we want to be able to update the various settings via the
             Account object?  Does it even make sense?
    """

    def __init__(self, data):
        self.account_status = data['stat']
        account = data['account']
        self.monitor_limit = int(account['monitorLimit'])
        self.monitor_interval = int(account['monitorInterval'])
        self.up_monitors = int(account['upMonitors'])
        self.down_monitors = int(account['downMonitors'])
        self.paused_monitors = int(account['pausedMonitors'])

    def status(self):
        """ Return the account status """
        return self.account_status

    def limit(self):
        """ Return the maximum number of monitors permitted for the acccount """
        return self.monitor_limit

    def interval(self):
        """ Return the monitoring interval """
        return self.monitor_interval

    def up(self):
        """ Return the number of monitors that are up """
        return self.up_monitors

    def down(self):
        """ Return the number of monitors that are down """
        return self.down_monitors

    def paused(self):
        """ Return the number of monitors that are paused """
        return self.paused_monitors

# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 John Hampton <pacopablo@pacopablo.com>
# All rights reserved.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution.
#
# Author: John Hampton <pacopablo@pacopablo.com>

import requests

from .account import Account
from .errors import (UptimeRobotInvalidAPIMethod, UptimeRobotInvalidAPIVersion,
                     UptimeRobotNoAPIKey)


UPTIMEROBOT_API_URL = {
    1: 'https://api.uptimerobot.com',
    2: 'https://api.uptimerobot.com/v2',
}

API_METHODS = [
    'getAccountDetails',
]

'''
Basic idea of how it will work is that
'''

class UptimeRobot(object):
    """ API class for the UptimeRobot API.  (https://uptimerobot.com/api)

    :param apiKey: UptimeRobot API key.  This is the account specific apiKey.
                   it can be specified on API creation or afterwards via
                   :method set_apikey:
    """

    def __init__(self, apiKey='', version=1):
        self.apiKey = apiKey
        if not version in UPTIMEROBOT_API_URL.keys():
            raise UptimeRobotInvalidAPIVersion
        self.api_version = version

    def _call_api(self, method, params=None, data=None):
        """ Internal method that prepars and executes the HTTP request.

        :param method: UptimeRobot API method being called. Must be in
                       API_METHODS
        :param params: dictionary of key/value pairs that are passed as HTTP
                       GET parameters.  The apiKey parameter is automatically
                       added.
        :return: returns the parsed JSON object
        """
        if method not in API_METHODS:
            raise UptimeRobotInvalidAPIMethod(method)

        if not self.apiKey:
            raise UptimeRobotNoAPIKey

        url = UPTIMEROBOT_API_URL[self.api_version] + '/' + method
        get_params = dict(params) if params else dict()
        get_params['apiKey'] = self.apiKey
        get_params['noJsonCallback'] = 1
        get_params['format'] = 'json'
        if data:
            r = requests.post(url, params=get_params, data=data)
        else:
            r = requests.get(url, params=get_params)
        if r.status_code != 200:
            return r.status_code, r.text
        return r.status_code, r.json()

    def get_account_details(self):
        """ Return an Account object for the UptimeRobot account """

        status, data = self._call_api('getAccountDetails')
        # .. TODO: need to handle the case where the call doesn't succeed
        return Account(data)



